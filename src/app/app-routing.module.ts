import { Component, NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';
import { LoginComponent } from './login/login.component';
import {PrimaryComponent} from "./primary-module/primary.component";
import {RouteGuardService} from "./services/route-guard.service";
import {ListDevisComponent} from "./primary-module/devis-components/list-devis/list-devis.component";

export const childRoutes = [
  {
  path: 'primary-module',
  loadChildren: () => import('./primary-module/primary.module').then(m => m.PrimaryModule)
},]as Route[];

const routes: Routes = [
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full',  },
  { path: 'login', component: LoginComponent},
  {
    path: 'GLV',
    component: PrimaryComponent,
    //canActivate: [RouteGuardService],
    children: childRoutes
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
