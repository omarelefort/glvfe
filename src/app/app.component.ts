import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  title = 'GLV';
  items: MenuItem[];

  constructor(){
    this.items =[
      {
        label:"Devis",
        icon: 'pi pi-fw pi-book',
        items:[
          {
            label:"Liste des devis ",
            icon: 'pi pi-fw pi-info-circle',
            routerLink:"/listDevis",},

            {

            label: 'Ajouter un devis',
            icon: 'pi pi-fw pi-plus',
            routerLink:"/welcome",  },

          ]
        },

             {   label:"Clients",
             icon: 'pi pi-fw pi-users',
                items:[
                  {
                    label:"Liste des clients ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/listClient",  },

                    {
                      label: 'Ajouter un client',
                    icon: 'pi pi-fw pi-user-plus',
                    routerLink:"/addClient/0",

                  },

                  ]
                },

                {

                label:"Utilisateurs",
                icon: 'pi pi-fw pi-user',

                items:[
                  {
                    label:"Liste des utilisateurs ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/listUsers", },

                    {
                      label: 'Ajouter utilisateur',
                    icon: 'pi pi-fw pi-user-plus',
                    routerLink:"/addUser/0",  }
                  ]
                },


             {   label:"Prix",
             icon: 'pi pi-fw pi-money-bill',
                items:[
                  {
                    label:"Liste des prix ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/listPrix",  },
                               

                  ]
                }

            ];
        }
    }
