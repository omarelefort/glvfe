import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  username = ''
  password = ''
  errorMsg='Invalid Credentials'
  invalidLogin=false
  hide = true;
  showloader=false
  constructor(private router: Router,
              private Auth:AuthenticationService,
              private messageService:MessageService) { }

  ngOnInit(): void {
    sessionStorage.clear();
    localStorage.clear();
  }
  handleLogin(){
    this.showloader = true;
  this.Auth.Login(this.username,this.password).subscribe(
    (response) =>{
      setTimeout(()=>{
        if(response){
          this.showloader=false;
        }
        this.router.navigate(['GLV/primary-module/listDevis']); },2000)
      console.log(response)

    },
    (error) =>{
      this.showloader=false;
      this.messageService.add({
        severity:'error',
        summary:"Message d'erreur",
        detail:"Login ou mot de passe incorrects"
      });
    }
  )

  }
}
