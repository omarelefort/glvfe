import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoirePrixComponent } from './accessoire-prix.component';

describe('AccessoirePrixComponent', () => {
  let component: AccessoirePrixComponent;
  let fixture: ComponentFixture<AccessoirePrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoirePrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessoirePrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
