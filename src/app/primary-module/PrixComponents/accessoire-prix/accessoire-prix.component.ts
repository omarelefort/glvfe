import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Accessoire, AccessoireService} from '../../../services/accessoire.service';
import { DialogService } from 'primeng/dynamicdialog';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {ChangementPrixComponent} from "../../popups/changement-prix/changement-prix.component";

@Component({
  selector: 'app-accessoire-prix',
  templateUrl: './accessoire-prix.component.html',
  styleUrls: ['./accessoire-prix.component.scss'],
  providers: [DialogService]
})
export class AccessoirePrixComponent implements OnInit {

  accessoire:any[];
  loading: boolean = true;
  constructor(private AccessoireService:AccessoireService,
              private route:Router,
              private dialogService: DialogService) { }

  ref: DynamicDialogRef;

  ngOnInit(): void {
    this.RetrieveAllAccessoires();
    this.loading = false;
  }
  RetrieveAllAccessoires(){
    this.AccessoireService.getAllAccessoire().
    subscribe(
      (data: Accessoire[]) => {
       this.accessoire = data;}

    )
  }
  ModifierAccessoire(SelectedAccessoire:Accessoire){
    this.ref = this.dialogService.open(ChangementPrixComponent, {
      header: 'Veuillez Entrez la nouvelle valeur',
      width: '50%'
    });
    this.ref.onClose.subscribe((prix: any) =>{
      if (prix != undefined) {
        SelectedAccessoire.prixA = prix;
        this.AccessoireService.updateAccessoire(SelectedAccessoire)
          .subscribe();
      }
    });
  }
}
