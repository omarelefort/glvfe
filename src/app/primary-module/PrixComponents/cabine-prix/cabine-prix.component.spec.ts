import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinePrixComponent } from './cabine-prix.component';

describe('CabinePrixComponent', () => {
  let component: CabinePrixComponent;
  let fixture: ComponentFixture<CabinePrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CabinePrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinePrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
