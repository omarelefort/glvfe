import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import { ChangementPrixComponent } from 'src/app/primary-module/popups/changement-prix/changement-prix.component';
import {Cabine, CabineService} from '../../../services/cabine.service';

@Component({
  selector: 'app-cabine-prix',
  templateUrl: './cabine-prix.component.html',
  styleUrls: ['./cabine-prix.component.scss'],
  providers: [DialogService]
})
export class CabinePrixComponent implements OnInit {
  cabine:Cabine[];
  loading: boolean = true;

  constructor(private CabineService:CabineService,
             private route:Router,
             private dialogService: DialogService) { }
             ref: DynamicDialogRef;
  ngOnInit(): void {
    this.RetrieveAllCabine();
    this.loading = false;
  }
  RetrieveAllCabine(){
    this.CabineService.getAllCabine().
     subscribe(
     (data: Cabine[]) => {
    this.cabine = data;
  }
     )
}
Modifier(SelectedCabine:Cabine){
  this.ref = this.dialogService.open(ChangementPrixComponent, {
    header: 'Veuillez Entrez la nouvelle valeur',
    width: '50%'
  });
  this.ref.onClose.subscribe((prix: any) =>{
    if (prix != undefined) {
      SelectedCabine.prix = prix;
      this.CabineService.UpdateCabine(SelectedCabine)
        .subscribe();
      }
    });
  }
}
