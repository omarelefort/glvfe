import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeArretPrixComponent } from './charge-arret-prix.component';

describe('ChargeArretPrixComponent', () => {
  let component: ChargeArretPrixComponent;
  let fixture: ComponentFixture<ChargeArretPrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChargeArretPrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeArretPrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
