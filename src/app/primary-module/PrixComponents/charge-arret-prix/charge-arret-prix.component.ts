
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ChangementPrixComponent } from 'src/app/primary-module/popups/changement-prix/changement-prix.component';
import {ChargeArret, ChargeArretService} from '../../../services/charge-arret.service';

@Component({
  selector: 'app-charge-arret-prix',
  templateUrl: './charge-arret-prix.component.html',
  styleUrls: ['./charge-arret-prix.component.scss'],
  providers: [DialogService]
})
export class ChargeArretPrixComponent implements OnInit {

chargeArret:ChargeArret[];
  loading: boolean= true;
  constructor(private ChargeArretService:ChargeArretService,
              private route:Router,
              private dialogService: DialogService) { }

              ref: DynamicDialogRef;
  ngOnInit(): void {
    this.RetrieveAllChargeArret();
    this.loading = false;
  }

  RetrieveAllChargeArret(){
    this.ChargeArretService.getAllchargeArret().
    subscribe(
      (data: ChargeArret[]) => {
       this.chargeArret = data;}

    )
  }
  ModifierChargeArret(SelectedChargeArret:ChargeArret){

      this.ref = this.dialogService.open(ChangementPrixComponent, {
        header: 'Veuillez Entrez la nouvelle valeur',
        width: '50%'
      });
      this.ref.onClose.subscribe((prix: any) =>{
        if (prix != undefined) {
          SelectedChargeArret.prixCh = prix;
          this.ChargeArretService.UpdatechargeArret(SelectedChargeArret)
            .subscribe();
        }
      });
    }
  AfficherDetails(){}
}
