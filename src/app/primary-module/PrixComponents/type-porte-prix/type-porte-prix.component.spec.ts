import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypePortePrixComponent } from './type-porte-prix.component';

describe('TypePortePrixComponent', () => {
  let component: TypePortePrixComponent;
  let fixture: ComponentFixture<TypePortePrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypePortePrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypePortePrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
