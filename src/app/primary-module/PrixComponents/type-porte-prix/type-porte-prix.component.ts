import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import { ChangementPrixComponent } from 'src/app/primary-module/popups/changement-prix/changement-prix.component';
import {TypePorte, TypePorteService} from '../../../services/type-porte.service';

@Component({
  selector: 'app-type-porte-prix',
  templateUrl: './type-porte-prix.component.html',
  styleUrls: ['./type-porte-prix.component.scss'],
  providers: [DialogService]
})
export class TypePortePrixComponent implements OnInit {
  loading: boolean = true;
  typePorte: TypePorte[];

  constructor(private TypePorteService:TypePorteService,
    private route:Router,
    private dialogService: DialogService) { }

    ref: DynamicDialogRef;

  ngOnInit(): void {
    this.RetrieveAllTypePorte();
    this.loading = false;
  }
  RetrieveAllTypePorte(){
    this.TypePorteService.getAllTypePorte().
     subscribe(
     (data: TypePorte[]) => {
    this.typePorte = data;
  }
     )
  }
  ModifierTypePorte(SelectedTypePorte:TypePorte){
    this.ref = this.dialogService.open(ChangementPrixComponent, {
      header: 'Veuillez Entrez la nouvelle valeur',
      width: '50%'
    });
    this.ref.onClose.subscribe((prix: any) =>{
      if (prix != undefined) {
        SelectedTypePorte.prixTypeP = prix;
        this.TypePorteService.UpdateTypePorte(SelectedTypePorte)
          .subscribe();
      }
    });
  }
}
