import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GetApisService } from '../../../services/get-apis.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Devis, DevisService } from '../../../services/devis.service';
import { User, UserService } from 'src/app/services/user.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Client, ClientService } from 'src/app/services/client.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss'],
  providers: [MessageService]
})
export class AddClientComponent implements OnInit {

  client:Client;
  btnname: string
  constructor(private clientService:ClientService,
              private router:Router,
              private messageService:MessageService,
              private route:ActivatedRoute,
              ){

  }





  ngOnInit() : void {

    if (this.route.snapshot.params['updateElement'] === '0') {
      var today = new Date();
    this.client = new Client(null,'Prospect','','','','','',
    '','','','','','','','','',today,
    '','','','','','','','','')
    this.btnname = "Ajouter"
    }
    else {
      this.clientService.getClientById(this.route.snapshot.params['updateElement']).
        subscribe(
          (data: any) => {
            this.client = data;
          }
        )
      this.btnname = "Modifier"
    }

  }

  addClient(){
  
    if (this.route.snapshot.params['updateElement'] === "0") {
    var response;
    this.clientService.AddClient(this.client).subscribe(
      data => { response= data
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Client Ajouté avec succes'})
        setTimeout(() => {this.router.navigate(['GLV/primary-module/listClient'])},2000 )
        
        },

    )
  }
  else {
    this.clientService.ModifierClient(this.client).subscribe(
      response => {
       
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Client Modifié avec succes'});
        setTimeout(() => {this.router.navigate(['GLV/primary-module/listClient'])},2000 )
        
      }
    );
  }
  this.messageService.clear('c');
}
}
