import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Client } from 'src/app/services/client.service';

@Component({
  selector: 'app-detail-client',
  templateUrl: './detail-client.component.html',
  styleUrls: ['./detail-client.component.scss']
})
export class DetailClientComponent implements OnInit {
 client:Client;
  constructor(
    public config: DynamicDialogConfig,
               public ref: DynamicDialogRef) {
                 this.client = config.data.SelectedClient; 
                console.log(this.client);
              }

  ngOnInit(): void {
  }
  list(){
    this.ref.close();
  }
}
