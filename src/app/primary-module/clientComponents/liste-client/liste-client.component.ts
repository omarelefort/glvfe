import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {Client, ClientService} from '../../../services/client.service';
import { MenuItem } from 'primeng/api';
import { DetailDevisComponent } from '../../devis-components/detail-devis/detail-devis.component';
import { DetailClientComponent } from '../detail-client/detail-client.component';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-liste-client',
  templateUrl: './liste-client.component.html',
  styleUrls: ['./liste-client.component.scss'],
  providers: [DialogService]
})
export class ListeClientComponent implements OnInit {



  clients:Client[];
  loading: boolean = true;


  constructor(private ClientService:ClientService,
              private route:Router , 
              private dialogService: DialogService) { }
              ref: DynamicDialogRef;

  ngOnInit(): void {
 this.RetrieveAllClient();
 this.loading = false;


  }

  RetrieveAllClient(){
    this.ClientService.getAllClient().
    subscribe(
      (data: Client[]) => {
       this.clients = data;}

    )
  }


  Modifier(SelectedClient:Client){
    this.route.navigate(['GLV/primary-module/addClient',SelectedClient.id])
  }

  Supprimer(SelectedClient){
    this.ClientService.DeleteClient(SelectedClient).
    subscribe(
      response => this.RetrieveAllClient()
    );
  }
  AfficherDetails(SelectedClient){
    this.ref = this.dialogService.open(DetailClientComponent, {
      header: 'Détails Du client',
      width: '80%',
      data: {SelectedClient:SelectedClient }
    });
  }


  
}
