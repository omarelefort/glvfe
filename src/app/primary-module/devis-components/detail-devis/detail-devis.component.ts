import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Devis } from 'src/app/services/devis.service';


@Component({
  selector: 'app-detail-devis',
  templateUrl: './detail-devis.component.html',
  styleUrls: ['./detail-devis.component.scss'],
  providers: [DatePipe]
})
export class DetailDevisComponent implements OnInit {

  devis: Devis;
  loading: boolean = true;


  constructor( public config: DynamicDialogConfig,
               public ref: DynamicDialogRef,
               public datepipe: DatePipe) {
                 this.devis = config.data.SelectedDevis;
                console.log(this.devis);
                }

  ngOnInit(): void {




  }
  exportPdf(){

  }

  list(){
    this.ref.close();
  }
  print() {
    let dimCabin = this.devis.chargeArrets.charge.dimensionCabine.split('x')
    let popupWin;
    let tva = this.devis.prix *0.2;
    let prixTva = this.devis.prix *1.2;

    popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
    popupWin.document.open();
    popupWin.document.write(`
      <!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- Favicon -->
  <link rel="icon" href="./images/favicon.png" type="image/x-icon" />

  <!-- Invoice styling -->
  <style>
    body {
      font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      text-align: center;
      color: #777;
    }

    body h1 {
      font-weight: 300;
      margin-bottom: 0px;
      padding-bottom: 0px;
      color: #000;
    }

    body h3 {
      font-weight: 300;
      margin-top: 10px;
      margin-bottom: 20px;
      font-style: italic;
      color: #555;
    }

    body a {
      color: #06f;
    }

    .invoice-box {
      max-width: 800px;
      margin: auto;
      padding: 30px;
      border: 1px solid #eee;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
      font-size: 16px;
      line-height: 24px;
      font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      color: #555;
    }

    .invoice-box table {
      width: 100%;
      line-height: inherit;
      text-align: left;
      border-collapse: collapse;
    }

    .invoice-box table td {
      padding: 5px;
      vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
      text-align: right;
    }

    .invoice-box table tr.top table td {
      padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
      font-size: 45px;
      line-height: 45px;
      color: #333;
    }

    .invoice-box table tr.information table td {
      padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
      background: #eee;
      border-bottom: 1px solid #ddd;
      font-weight: bold;
    }

    .invoice-box table tr.details td {
      padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
      border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
      border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
      border-top: 2px solid #eee;
      font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
      .invoice-box table tr.top table td {
        width: 100%;
        display: block;
        text-align: center;
      }

      .invoice-box table tr.information table td {
        width: 100%;
        display: block;
        text-align: center;
      }


    }
  </style>
</head>

<body>

  <br />

  <div class="invoice-box">
    <table>
      <tr class="top">
        <td colspan="2">
          <table>
            <tr>

              <td>
                Ascenseurs & Escaliers Roulants<br />
\t\t\t\t\t\t\t\t\twww.glv.ma<br />

              </td>
              <td class="title">
                <img src="https://glvmaroc.netlify.app/assets/glvPictures/glv_logo.png" alt="GLV logo" style="width: 100%; max-width: 200px" />
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr class="information">
        <td>
          <table>
            <tr>
              <td> 
              <span style="font-weight: bold">  Offre commerciale n° :</span> `+this.devis.matricule+` <br />
\t\t\t\t\t\t\t\t<span style="font-weight: bold">Date :</span> `+this.datepipe.transform(this.devis.dateDevis, 'dd/MM/yyyy') +`<br />
\t\t\t\t\t\t\t\t<span style="font-weight: bold">Client :</span> `+this.devis.client.nom+`<br />
<span style="font-weight: bold">Adresse :</span> `+this.devis.client.adresse+`<br />
<span style="font-weight: bold">Pays :</span> `+this.devis.client.pays+`<br />
              </td>


            </tr>
          </table>
        </td>
      </tr>

      <tr class="heading">
        <td style=" text-align: center">DIMENSIONS DE LA GAINE</td>

        <td></td>
      </tr>
    <tr class="item">
<span style="font-weight: bold">Pays :</span> `+this.devis.client.pays+`<br />
      <td><span style="font-weight: bold">Type de gaine : </span>`+this.devis.typeGaine+`</td>
    </tr>
      <tr class="item">
      <td><span style="font-weight: bold">Type :</span> Ascensseur</td>
    </tr>

    <tr class="item">
    <td><span style="font-weight: bold"> Largeur :</span>`+this.devis.dimensionGaineL +` mm</td>
  </tr>


  <tr class="item">
  <td><span style="font-weight: bold"> Profondeur :</span> `+this.devis.dimensionGaineP +` mm</td>
</tr>


<tr class="item">
<td><span style="font-weight: bold">  Hauteur sous dalle :</span> `+this.devis.hauteurSousDalle +` mm</td>
</tr>


<tr class="item">
<td><span style="font-weight: bold"> Cuvette :`+this.devis.cuvette +` mm</td>
</tr>
<tr class="item">
<td><span style="font-weight: bold"> Local machinerie : </span>`+this.devis.localMachinerie +`</td>
</tr>

        <td></td>
      </tr>

      <tr class="heading">
        <td style="text-align:center">CARACTERISTIQUES PRINCIPALES DE L'ASCENSEUR</td>

        <td></td>
      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Provenance : </span> `+this.devis.typeInstall +`</td>


      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Normes et directives :</span> Tous nos ascenseurs sont conformes à la norme EN 81-1 et les composants de sécurité selon la directive européenne 2014/33/UE </td>
      </tr>
       <tr class="item">
        <td><span style="font-weight: bold">Usage : </span>  `+ this.devis.usageAsc +`</td>
      </tr>
      <tr class="item ">
        <td><span style="font-weight: bold">Quantité : </span>  `+ this.devis.quantite +`</td>
      </tr>
      <tr class="item ">
        <td><span style="font-weight: bold">Charge Utile : </span> `+this.devis.chargeArrets.charge.chargeUtile +` </td>
      </tr>

      <tr class="item ">
        <td><span style="font-weight: bold">Vitesse : </span> `+this.devis.vitesse +` </td>
      </tr>

      <tr class="item ">
        <td><span style="font-weight: bold">Mode d'entrainement : </span> `+this.devis.modeEntrainement +`</td>
      </tr>

      <tr class="item ">
        <td><span style="font-weight: bold">Arrêts : </span> `+ this.devis.chargeArrets.arret.nbArret +`</td>
      </tr>

      <tr class="item ">
        <td><span style="font-weight: bold">Accès : </span>`+ this.devis.acces +`</td>
      </tr>

      <tr class="item ">
        <td><span style="font-weight: bold">Course : </span> `+this.devis.course +` mètres</td>
      </tr>
      <tr class="item ">
        <td><span style="font-weight: bold">Type de Manoeuvre : </span> `+this.devis.typeManoeuvre +`</td>
      </tr><tr class="item ">
        <td><span style="font-weight: bold">Alimentation Moteur/ éclairage : </span> `+this.devis.alimentationMoteur +` / 220 V 50 Hz</td>
      </tr>

      <tr class="total">
        <td></td>
      <tr class="heading">
        <td style="text-align:center">CARACTERISTIQUES DE LA CABINE</td>

        <td></td>
      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Modèle :</span> `+this.devis.cabine.modelCabine +`</td>


      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Finition :</span> `+this.devis.finition +`</td>
      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Largeur :</span> `+dimCabin[0]+` </td>
      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Profondeur :</span> `+dimCabin[1]+`</td>
      </tr>

      <tr class="item">
        <td><span style="font-weight: bold">Hauteur :</span> `+dimCabin[2]+`</td>
      </tr>
       <tr class="item">
        <td><span style="font-weight: bold">Sol : `+this.devis.sol +`</td>
      </tr>
      <tr class="item">
        <td><span style="font-weight: bold">Plafond :</span> `+this.devis.plafond +`</td>
      </tr>
      <tr class="item">
        <td><span style="font-weight: bold">Miroir :</span> `+this.devis.miroir +`</td>
      </tr>
      <tr class="heading">
        <td style="text-align:center">CARACTERISTIQUES DES PORTES PALIERES</td>
        <tr class="item">
        <td><span style="font-weight: bold">Type de porte :</span> `+this.devis.typePorte.porteCabine +`</td>
      </tr>
      <tr class="item">
        <td><span style="font-weight: bold">A Ouverture :</span> `+this.devis.ouverture +`</td>
      </tr>



      <tr class="item">
        <td><span style="font-weight: bold">Degré de résistance au feu : E-120 minutes selon norme EN 81-58 </td>
      </tr>

       <tr class="heading">
        <td style="text-align:center">SIGNALISATIONS ET ACCESSOIRES STANDARD</td>
        <tr class="item">
        <td>Indicateur de position en cabine type LCD bleu avec flèches de sens et de prochain départ</td>
      </tr>
       <tr class="item">
        <td>Voyant lumineux à LED pour l'enregistrement de l'appel </td>
      </tr>

<tr class="item">
        <td>Plaque indiquant la charge autorisée</td>
      </tr>

      <tr class="item">
        <td> Bouton de réouverture </td>
      </tr>

      <tr class="item">
        <td> Eclairage de secours à 1h d'autonomie </td>
      </tr>

      <tr class="item">
        <td>Indicateur de position cabine à l'étage principal type LCD bleu </td>
      </tr>

      <tr class="item">
        <td>Sirène d'alarme électronique de 104 dB de puissance, actionnable par bouton dans la cabine</td>
      </tr>

      <tr class="item">
        <td>Boutons anti-vandales avec inscription braille pour les non-voyants </td>
      </tr>


      <tr class="item">
        <td>Eclairage de gaine </td>
      </tr>

      <tr class="item">
        <td>Echelle d'accès en cuvette </td>
      </tr>

      <tr class="item">
        <td>Rideau infrarouge et réouverture automatique sur obstacle </td>
      </tr>

      <tr class="item">
        <td>Ventilation naturelle de la cabine à travers les plinthes </td>
      </tr>


      <tr class="item">
        <td>Alimentation de secours de 12V pour sirène d'alarme et éclairage de secours </td>
      </tr>


      <tr class="item">
        <td>Protection de tous les éléments tournants au local machinerie</td>
      </tr>


      <tr class="item">
        <td>Balustrade de protection sur le toit de la cabine</td>
      </tr>



        <td></td>
      </tr>
      <tr class="heading">
        <td style="text-align:center">CONDITIONS DE VENTE</td>

        <td></td>
      </tr>

<tr>


      </tr>

      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Validité de l'offre:</span> Cette offre
          commerciale est valable au </td>
      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Conditions de paiement:</span> <br>
        40% à la commande et signature du présent devis <br>
        20% à la pose des guides et portes <br>
        30% à la livraison du complément du matériel <br>
        10% à la mise en marche et réception<br>
        </td>
      </tr>
      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Mode de paiement:</span> <br>
          Les paiements s’effectuent sans escompte par chèque bancaire ou par virement
        </td>
      </tr>

      <tr class="item">
        <td> <span style="font-weight: bold; text-decoration: underline">Délai de livraison:</span> de la date du
          signature du présent devis, règlement du 1er acompte de commande et confirmation des choix de finition et
          couleur de la cabine, et si le délai de livraison coïncide avec la période de fermeture de notre usine pour
          congés annuel, à savoir quatre semaines en août et deux semaines en décembre, notre délai serait prolongé
          respectivement de la même durée de fermeture de notre fournisseur </td>
      </tr>
      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Durée d'installation</span> <br>
         6 semaines par appareil, à condition que tous les travaux préparatoires soient réalisés tels qu'une gaine complètement aménagée et propre, local machinerie fermé, disponibilité d'un courant stable de 380V, mise à disposition d'un local fermé d'une superficie de 20 m² à côté de la gaine pour le stockage de notre matériel, etc.…
        </td>

        <td></td>
      </tr>


      </tr>


      </tr>



      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Réception et contrôle technique:</span> Avant la
          mise en marche, il sera procédé à une vérification technique de l'appareil par un organisme de contrôle agréé
          par l'état dont les frais sont à la charge de la société GLV, cette dernière est considérée comme réception
          définitive de l'appareil </td>

      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Garantie:</span> 01 année contre tout vice de
          matière ou fabrication à partir de la date de réception sous réserve, l'entretien de l'appareil par nos soins,
          les défectuosités résultant d'une mauvaise manipulation ou utilisation anormale ne sont pas compris dans la
          garantie. L'intervention d'une personne ou société tiers sur l’appareil en question suspend immédiatement
          cette garantie.</td>
      <tr class="item">
        <td><span style="font-weight: bold; text-decoration: underline">Entretien:</span> la mise en marche, il sera
          signé un contrat d'entretien annuel dont les premiers 06 mois sont gratuits.
        </td>

      </tr> <br/> <br/>
<table style="width:70%; margin: auto; border: 1px solid black; border-collapse: collapse;">

  <tr>
    <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">Montant Unitaire H.T</th>
    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">`+this.devis.prix+` MAD</th>
  </tr>
  <tr>
    <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">Montant Total H.T</th>
    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">`+this.devis.prix+` MAD</td>
  </tr>
  <tr>
    <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">T.V.A 20%</td>
    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">`+tva+` MAD</td>
  </tr>
   <tr>
    <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">Montant T.T.C</td>
    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;text-align: left;">`+prixTva+` MAD</td>
  </tr>
</table>
<br>
<table>
      <tr class="heading">
        <td style="text-align:center">SIGNATURES</td>
        </tr> 
        <table style="width:100%; margin: auto;text-align:center;">

        <tr>
          <th style="text-align:center" >GLV s.a.r.l</th>
          <th style="text-align:center">Ste: </th>
        </tr>
        <tr>
          <td style="text-align:center">Responsable commercial</td>
          <td style="text-align:center">Lu et approuvé</td>
        </tr>
    <tr></tr>

        </table>

    </table>

  </div>


</body>

</html>`);
    /* window.close(); */
    popupWin.document.close();
  }
}
