import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevisPDFComponent } from './devis-pdf.component';

describe('DevisPDFComponent', () => {
  let component: DevisPDFComponent;
  let fixture: ComponentFixture<DevisPDFComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevisPDFComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevisPDFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
