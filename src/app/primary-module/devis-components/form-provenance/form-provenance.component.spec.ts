import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormProvenanceComponent } from './form-provenance.component';

describe('FormProvenanceComponent', () => {
  let component: FormProvenanceComponent;
  let fixture: ComponentFixture<FormProvenanceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProvenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProvenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
