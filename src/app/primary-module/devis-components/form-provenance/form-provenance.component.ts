import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-provenance',
  templateUrl: './form-provenance.component.html',
  styleUrls: ['./form-provenance.component.scss']
})
export class FormProvenanceComponent implements OnInit {
 typeI: string
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {

     this.typeI = this.route.snapshot.params['typeinstall']
    
  }

}
                  