import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormSuiteComponent } from './form-suite.component';

describe('FormSuiteComponent', () => {
  let component: FormSuiteComponent;
  let fixture: ComponentFixture<FormSuiteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSuiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
