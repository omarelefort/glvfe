import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GetApisService } from '../../../services/get-apis.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Devis, DevisService } from '../../../services/devis.service';
import { User, UserService } from 'src/app/services/user.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {Accessoire, AccessoireService} from "../../../services/accessoire.service";
import {Cabine, CabineService} from "../../../services/cabine.service";
import {ChargeArret, ChargeArretService} from "../../../services/charge-arret.service";
import {TypePorte, TypePorteService} from "../../../services/type-porte.service";
import {Charge, ChargeService} from "../../../services/charge.service";
import {Arret, ArretService} from "../../../services/arret.service";
import {Client, ClientService} from "../../../services/client.service";
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';


@Component({
  selector: 'app-form-suite',
  templateUrl: './form-suite.component.html',
  styleUrls: ['./form-suite.component.scss'],
  providers: [MessageService]
})
export class FormSuiteComponent implements OnInit {

  devis:Devis;
  user:User;
  ChargeU :Charge [];
  nbrArret :Arret [];
  Accessoires:Accessoire[];
  Modele :Cabine [];
  typePorte: TypePorte[];
  clients: Client[];
  modeE = [];
  LocalM = [];
  AccesA = [];
  Vitesse = [];
  Finition = [];
  entretiens = [];
  delaiLivr = [];
  garantie = [];
  LocalActivated = true;
  provenanceC : string;
  btnname : string;
  SelectedCharge: Charge;
  SelectedArret: Arret;
  form: FormGroup = new FormGroup({});
  TypeGaine = [];
  Usage = [];
  TypeM = [];
  AlimentationM = [];
  Sol = [];
  Plafond = [];
  Ouverture = [];
  TypeManoeuvre = [];
  Miroir = []

  constructor(private formBuilder: FormBuilder,
              private getapis:GetApisService,
              private route:ActivatedRoute,
              private DevisService:DevisService,
              private userService:UserService,
              private authService:AuthenticationService,
              private accessoireService:AccessoireService,
              private cabineService:CabineService,
              private chargeArretService:ChargeArretService,
              private typePorteService:TypePorteService,
              private chargeService:ChargeService,
              private arretService:ArretService,
              private clientService:ClientService,
              private router:Router,
              private messageService:MessageService,) {
   }

  ngOnInit(): void {

    this.userService.getUserByUsername(this.authService.getAuthenticatedUser()).subscribe(
      (response:User) => {
        this.user = response;
      }
    )
        if(this.route.snapshot.params['updateElement'] === '0')
        {
          this.SelectedCharge = new Charge(null,'',null,'','',null)
          this.SelectedArret = new Arret(null,'')
          this.devis = new Devis(null,'',null,'Ascensseur',null,
          '',null,null,'','',null,'','',''
          ,'','',null,'','','',this.user,new Client(null,'','','','',''
          ,'','','','','','','','','','',null,'','','','','','','','',''),
          new ChargeArret(null,null,new Charge(null,'',null,'','',null),null),
          new Cabine(null,'',null,'',null,'','',''),
            new TypePorte(null,'',null),
            null,null,'','','',null,'','','','')
          this.btnname = "Ajouter"
          this.LocalActivated = true}

        else {
          this.DevisService.getDevisById(this.route.snapshot.params['updateElement']).
              subscribe(
                (data: any) => {
                  this.devis = data;
                  this.selectLocalM(this.devis.modeEntrainement)
                  this.SelectedArret = this.devis.chargeArrets.arret
                  this.SelectedCharge = this.devis.chargeArrets.charge
                }
              )
              this.LocalActivated = false
              this.btnname = "Modifier"
        }
    this.form = this.formBuilder.group({

      number: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]

    })
    this.getapis.getPosts('asc/modeEntrainement').
              subscribe(
                (data: any) => {
                  this.modeE = data;
                }
                )
    this.getapis.getPosts('asc/typeManoeuvre').
    subscribe(
      (data: any) => {
        this.TypeManoeuvre = data;
      }
    )
    this.getapis.getPosts('asc/usage').
    subscribe(
      (data: any) => {
        this.Usage = data;
      }
    )
    this.getapis.getPosts('asc/alimentationMoteur').
    subscribe(
      (data: any) => {
        this.AlimentationM = data;
      }
    )
    this.getapis.getPosts('asc/sol').
    subscribe(
      (data: any) => {
        this.Sol = data;
      }
    )
    this.getapis.getPosts('asc/plafond').
    subscribe(
      (data: any) => {
        this.Plafond = data;
      }
    )
    this.getapis.getPosts('asc/ouverture').
    subscribe(
      (data: any) => {
        this.Ouverture = data;
      }
    )
    this.getapis.getPosts('asc/miroir').
    subscribe(
      (data: any) => {
        this.Miroir = data;
      }
    )
    this.getapis.getPosts('asc/typeGaine').
    subscribe(
      (data: any) => {
        this.TypeGaine = data;
      }
    )
    this.chargeService.getAllCharges().
              subscribe(
                (data: any) => {
                  this.ChargeU = data;
                }
              )
    this.clientService.getAllClient().subscribe(
      (data:any) => {
        this.clients = data
      }
    )
    this.getapis.getPosts('asc/acces').
              subscribe(
                (data: any) => {
                  this.AccesA = data;
                }
              )

    this.typePorteService.getAllTypePorte().subscribe(
      (data:any) => {
          this.typePorte = data;
      }
    )
    this.arretService.getAllArret().
              subscribe(
                (data: any) => {
                  this.nbrArret = data;
                }
              )

    this.getapis.getPosts('asc/vitesse').
              subscribe(
                (data: any) => {
                  this.Vitesse = data;
                }
              )
    this.getapis.getPosts('asc/entretien').
    subscribe(
      (data: any) => {
        this.entretiens = data;
      }
    )

    this.accessoireService.getAllAccessoire().
              subscribe(
                (data: any) => {
                  this.Accessoires = data;
                }
              )
    this.getapis.getPosts('asc/finition').
              subscribe(
                (data: any) => {
                  this.Finition = data;
                }
              )

    switch (this.route.snapshot.params['provenance'])
    {
                case "1":
                  this.provenanceC = "Italie";
                    break;
                case "2":
                  this.provenanceC = "Espagne";
                    break;
                case "3":
                  this.provenanceC = "Europe";
                    break;
              }
        if(this.provenanceC === "Italie") {
          this.getapis.getPosts('asc/delaiLivraisonItaly').subscribe(
            (data: any) => {
              this.delaiLivr = data
              this.devis.delaiLivraison = this.delaiLivr[0];
            }
          )
          this.getapis.getPosts('asc/garantieItaly').subscribe(
            (data: any) => {
              this.garantie = data
              this.devis.garantie = this.garantie[0];
            }
          )
        }
    else if (this.provenanceC === "Espagne")
        {
          this.getapis.getPosts('asc/delaiLivraisonEsp').subscribe(
            (data: any) => {
              this.delaiLivr = data
              this.devis.delaiLivraison = this.delaiLivr[0];
            }
          )
          this.getapis.getPosts('asc/garantieEsp').subscribe(
            (data: any) => {
              this.garantie = data
              this.devis.garantie = this.garantie[0];
            }
          )

        }
     this.cabineService.getCabineByProvenance(this.provenanceC).
              subscribe(
                (data: any) => {
                  this.Modele = data;
                }
              )

  }
  //Declencheur Local Machinerie par rapport au mode d'entrainement
  selectLocalM(localMach){
    console.log(this.devis)
    let typeM : String;
    switch (localMach) {
      case "Electrique 2V":
        typeM = "localMachinerie2v";
          break;
      case "Electrique 3VF":
        typeM = "localMachinerie3v";
          break;
      case "Machine Room Less":
        typeM = "mrl";
          break;
      case "Hydraulique":
        typeM = "localMachinerieHydr";
          break;
    }
    this. LocalActivated = false;
    this.getapis.getPosts('asc/'+typeM).
    subscribe(
      (data: any) => {
        this.LocalM = data;
      }
    )
  }
  getChargeArret(){
    if(this.SelectedCharge.id != null && this.SelectedArret.id != null)
    this.chargeArretService.getchargeArretByChargeArret(this.SelectedCharge.id,this.SelectedArret.id)
      .subscribe(
        (data:ChargeArret) => {
          this.devis.chargeArrets = data;
        }
      )
  }



//Declencheur Dimension porte par rapport au charge utile

  // handleSuccessfulResponce(response){
  //   this.modeE = response;
  //   console.log(this.modeE)
  // }
  submit(){

   if(this.route.snapshot.params['updateElement']=== "0")
   {
          this.devis.user = this.user
          this.DevisService.AddDevis(this.devis).subscribe(
            response => {
              this.messageService.add({severity:'success', summary: 'Success', detail: 'Devis Ajouté avec succes'})
              setTimeout(() => { this.router.navigate(['GLV/primary-module/listDevis'])},2000 )

            }
          );
        }
        else {

          this.devis.user = this.user
          this.DevisService.UpdateDevisById(this.devis).subscribe(
            response => {
              this.messageService.add({severity:'success', summary: 'Success', detail: 'Devis Modifié avec succes'})
              setTimeout(() => { this.router.navigate(['GLV/primary-module/listDevis'])},2000 )
            }
          );
        }
this.messageService.clear('c');
  }

}
