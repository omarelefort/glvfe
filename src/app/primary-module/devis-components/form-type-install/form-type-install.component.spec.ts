import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormTypeInstallComponent } from './form-type-install.component';

describe('FormTypeInstallComponent', () => {
  let component: FormTypeInstallComponent;
  let fixture: ComponentFixture<FormTypeInstallComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTypeInstallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTypeInstallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
