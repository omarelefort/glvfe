import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import {DevisService} from '../../../services/devis.service';
import {Devis} from '../../../services/devis.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { Router } from '@angular/router';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DetailDevisComponent } from '../detail-devis/detail-devis.component';


@Component({
  selector: 'app-list-devis',
  templateUrl: './list-devis.component.html',
  styleUrls: ['./list-devis.component.scss'],
  providers: [DialogService]
})

export class ListDevisComponent implements OnInit {




  devis:Devis[];

  loading: boolean = true;

  constructor(private DevisService:DevisService,
              private route:Router,
              private dialogService: DialogService) { }

              ref: DynamicDialogRef;

  ngOnInit(): void {
    this.RetrieveAllDevis();
    this.loading = false;
  }
  RetrieveAllDevis(){
    this.DevisService.getAllDevis().
    subscribe(
      (data: any) => {
          this.devis = data;  }
       // this.devis = new MatTableDataSource(data);

    )

  }
  Modifier(SelectedDevis:Devis){
  let provenanceC:string;
  switch (SelectedDevis.cabine.provenance) {
    case "Italie":
      provenanceC = "1";
        break;
    case "Espagne":
      provenanceC = "2";
        break;
    case "Europe":
      provenanceC = "3";
        break;
  }
  this.route.navigate(['GLV/primary-module/formS',provenanceC,1,SelectedDevis.id])
}
Supprimer(SelectedDevis){
  this.DevisService.DeleteDevisById(SelectedDevis).
  subscribe(
    response => this.RetrieveAllDevis()
  );
}

AfficherDetails(SelectedDevis){
  this.ref = this.dialogService.open(DetailDevisComponent, {
    header: 'Détails Du Devis',
    width: '80%',
    data: {SelectedDevis: SelectedDevis}
  });
}
confirmer(SelectedDevis){
    SelectedDevis.etat = "Confirmé"
  this.DevisService.UpdateDevisById(SelectedDevis).
  subscribe(
    response => this.RetrieveAllDevis()
  );
}

  retenir(SelectedDevis){
    SelectedDevis.etat = "Validé"
    this.DevisService.UpdateDevisById(SelectedDevis).
    subscribe(
      response => this.RetrieveAllDevis()
    );
  }

  annuler(SelectedDevis){
    SelectedDevis.etat = "Annulé"
    this.DevisService.UpdateDevisById(SelectedDevis).
    subscribe(
      response => this.RetrieveAllDevis()
    );
  }
  cloturer(SelectedDevis){
    SelectedDevis.etat = "Cloturé"
    this.DevisService.UpdateDevisById(SelectedDevis).
    subscribe(
      response => this.RetrieveAllDevis()
    );
  }


}
