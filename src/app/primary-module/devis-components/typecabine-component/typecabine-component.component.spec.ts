import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypecabineComponentComponent } from './typecabine-component.component';

describe('TypecabineComponentComponent', () => {
  let component: TypecabineComponentComponent;
  let fixture: ComponentFixture<TypecabineComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypecabineComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypecabineComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
