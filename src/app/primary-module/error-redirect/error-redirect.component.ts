import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-redirect',
  templateUrl: './error-redirect.component.html',
  styleUrls: ['./error-redirect.component.scss']
})
export class ErrorRedirectComponent implements OnInit {

  constructor() { }

  errorMsg = 'Unexpected error happened!!!'
  ngOnInit(): void {
  }

}
