import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangementPrixComponent } from './changement-prix.component';

describe('ChangementPrixComponent', () => {
  let component: ChangementPrixComponent;
  let fixture: ComponentFixture<ChangementPrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangementPrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangementPrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
