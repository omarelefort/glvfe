import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";

@Component({
  selector: 'app-changement-prix',
  templateUrl: './changement-prix.component.html',
  styleUrls: ['./changement-prix.component.scss']
})
export class ChangementPrixComponent implements OnInit {

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  prix: any;
  ngOnInit(): void {
  }

  submit(){
    this.ref.close(this.prix);
  }
  cancel(){
    this.ref.close(undefined);
  }
}
