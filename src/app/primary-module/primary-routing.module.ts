import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ListDevisComponent} from "./devis-components/list-devis/list-devis.component";
import {RouteGuardService} from "../services/route-guard.service";
import {ListeClientComponent} from "./clientComponents/liste-client/liste-client.component";
import {AddClientComponent} from "./clientComponents/add-client/add-client.component";
import {FormTypeInstallComponent} from "./devis-components/form-type-install/form-type-install.component";
import {FormProvenanceComponent} from "./devis-components/form-provenance/form-provenance.component";
import {FormSuiteComponent} from "./devis-components/form-suite/form-suite.component";
import {ListUsersComponent} from "./user-components/list-users/list-users.component";
import {AddUserComponent} from "./user-components/add-user/add-user.component";
import {ListPrixComponent} from "./PrixComponents/list-prix/list-prix.component";
import {AccessoirePrixComponent} from "./PrixComponents/accessoire-prix/accessoire-prix.component";
import {ChargeArretPrixComponent} from "./PrixComponents/charge-arret-prix/charge-arret-prix.component";
import {TypePortePrixComponent} from "./PrixComponents/type-porte-prix/type-porte-prix.component";
import {ErrorRedirectComponent} from "./error-redirect/error-redirect.component";
import { TypecabineComponentComponent } from "./devis-components/typecabine-component/typecabine-component.component";

export const  primaryRoutes: Routes = [
  { path: 'listDevis', component: ListDevisComponent,canActivate:[RouteGuardService]  },
  { path: 'listClient',component: ListeClientComponent,canActivate:[RouteGuardService] },
  { path: 'addClient/:updateElement',component: AddClientComponent,canActivate:[RouteGuardService] },
  { path: 'welcome', component: FormTypeInstallComponent,canActivate:[RouteGuardService] },
  { path: 'formP/:typeinstall', component: FormProvenanceComponent ,canActivate:[RouteGuardService] },
  { path: 'formS/:provenance/:typeinstall/:updateElement', component: FormSuiteComponent ,canActivate:[RouteGuardService] },
  { path: 'listUsers', component: ListUsersComponent,canActivate:[RouteGuardService]  },
  { path: 'addUser/:updateElement', component: AddUserComponent ,canActivate:[RouteGuardService] },
  { path: 'listPrix',component: ListPrixComponent,canActivate:[RouteGuardService] },
  { path: 'accessoirePrix',component: AccessoirePrixComponent,canActivate:[RouteGuardService] },
  { path: 'chargeArretPrix',component: ChargeArretPrixComponent,canActivate:[RouteGuardService] },
  { path: 'typePortePrix',component: TypePortePrixComponent,canActivate:[RouteGuardService] },
  { path: 'typeCabine', component: TypecabineComponentComponent,canActivate:[RouteGuardService]},
  { path: '**', component: ErrorRedirectComponent },
  
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(primaryRoutes)]
})
export class PrimaryRoutingModule { }
