import {Component, OnInit} from '@angular/core';
import { MenuItem } from 'primeng/api';
import {Router} from "@angular/router";

@Component({
  selector: 'app-primary',
  templateUrl: './primary.component.html',
  styleUrls: ['./primary.component.scss']
})


export class PrimaryComponent implements OnInit{
  title = 'GLV';
  items: MenuItem[];

  constructor(private router: Router){
    this.items =[
      {
        label:"Devis",
        icon: 'pi pi-fw pi-book',
        items:[
          {
            label:"Liste des devis ",
            icon: 'pi pi-fw pi-info-circle',
            routerLink:"/GLV/primary-module/listDevis",},

            {

            label: 'Ajouter un devis',
            icon: 'pi pi-fw pi-plus',
            routerLink:"/GLV/primary-module/welcome",  },

          ]
        },

             {   label:"Clients",
             icon: 'pi pi-fw pi-users',
                items:[
                  {
                    label:"Liste des clients ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/GLV/primary-module/listClient",  },

                    {
                      label: 'Ajouter un client',
                    icon: 'pi pi-fw pi-user-plus',
                    routerLink:"/GLV/primary-module/addClient/0",

                  },

                  ]
                },

                {

                label:"Utilisateurs",
                icon: 'pi pi-fw pi-user',

                items:[
                  {
                    label:"Liste des utilisateurs ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/GLV/primary-module/listUsers", },

                    {
                      label: 'Ajouter utilisateur',
                    icon: 'pi pi-fw pi-user-plus',
                    routerLink:"/GLV/primary-module/addUser/0",  }
                  ]
                },


             {   label:"Prix",
             icon: 'pi pi-fw pi-money-bill',
                items:[
                  {
                    label:"Liste des prix ",
                    icon: 'pi pi-fw pi-info-circle',
                    routerLink:"/GLV/primary-module/listPrix",  },


                  ]
                }

            ];
        }
        ngOnInit(): void {
        }

  logout(){
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigate(['login'])
  }
}
