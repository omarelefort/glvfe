import {NgModule , CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PrimaryRoutingModule} from "./primary-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {FormTypeInstallComponent} from "./devis-components/form-type-install/form-type-install.component";
import {ErrorRedirectComponent} from "./error-redirect/error-redirect.component";
import {ListDevisComponent} from "./devis-components/list-devis/list-devis.component";
import {FormProvenanceComponent} from "./devis-components/form-provenance/form-provenance.component";
import {FormSuiteComponent} from "./devis-components/form-suite/form-suite.component";
import {FooterComponent} from "./footer/footer.component";
import {ListUsersComponent} from "./user-components/list-users/list-users.component";
import {AddUserComponent} from "./user-components/add-user/add-user.component";
import {ListeClientComponent} from "./clientComponents/liste-client/liste-client.component";
import {AddClientComponent} from "./clientComponents/add-client/add-client.component";
import {ListPrixComponent} from "./PrixComponents/list-prix/list-prix.component";
import {AccessoirePrixComponent} from "./PrixComponents/accessoire-prix/accessoire-prix.component";
import {ChargeArretPrixComponent} from "./PrixComponents/charge-arret-prix/charge-arret-prix.component";
import {TypePortePrixComponent} from "./PrixComponents/type-porte-prix/type-porte-prix.component";
import {CabinePrixComponent} from "./PrixComponents/cabine-prix/cabine-prix.component";
import {ChangementPrixComponent} from "./popups/changement-prix/changement-prix.component";
import {DetailDevisComponent} from "./devis-components/detail-devis/detail-devis.component";
import {MenubarModule} from "primeng/menubar";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatMenuModule} from "@angular/material/menu";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatStepperModule} from "@angular/material/stepper";
import {MatSelectModule} from "@angular/material/select";
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {CascadeSelectModule} from "primeng/cascadeselect";
import {DropdownModule} from "primeng/dropdown";
import {MatRadioModule} from "@angular/material/radio";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {ButtonModule} from "primeng/button";
import {TableModule} from "primeng/table";
import {DynamicDialogModule} from "primeng/dynamicdialog";
import {PrimaryComponent} from "./primary.component";
import {RouterModule} from "@angular/router";
import {ToastModule} from 'primeng/toast';
import { DetailClientComponent } from './clientComponents/detail-client/detail-client.component';
import { DevisPDFComponent } from './devis-components/devis-pdf/devis-pdf.component';
import { TypecabineComponentComponent } from './devis-components/typecabine-component/typecabine-component.component';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    PrimaryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MenubarModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatSidenavModule,
    MatMenuModule,
    MatGridListModule,
    MatTooltipModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatSelectModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    CascadeSelectModule,
    DropdownModule,
    MatRadioModule,
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    TableModule,
    DynamicDialogModule,
    ToastModule,

  ],
  declarations: [
    PrimaryComponent,
    FormTypeInstallComponent,
    ErrorRedirectComponent,
    ListDevisComponent,
    FormProvenanceComponent,
    FormSuiteComponent,
    FooterComponent,
    ListUsersComponent,
    AddUserComponent,
    ListeClientComponent,
    AddClientComponent,
    ListPrixComponent,
    AccessoirePrixComponent,
    ChargeArretPrixComponent,
    TypePortePrixComponent,
    CabinePrixComponent,
    ChangementPrixComponent,
    DetailDevisComponent,
    DetailClientComponent,
    DevisPDFComponent,
    TypecabineComponentComponent,
 ],
  bootstrap: [PrimaryComponent],
  providers : []
})
export class PrimaryModule { }
