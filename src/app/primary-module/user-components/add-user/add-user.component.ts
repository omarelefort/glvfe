import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import {Role, User, UserService} from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  providers: [MessageService]
})
export class AddUserComponent implements OnInit {


  user: User
  userResp:User
  btnname: string
  WarnMsg:string
  role:Role[]
  loginExistant = false
  EmailExistant = false
  constructor(private route: ActivatedRoute,
    private UserService: UserService,
    private messageService:MessageService,
    private router: Router) {
  }


  ngOnInit(): void {
    this.UserService.getAllRoles().subscribe(
      (data:Role[])=> {
        this.role = data;
      }
    )
    if (this.route.snapshot.params['updateElement'] === '0') {
      this.user = new User(null, '', '',
        '', '', '',new Role(null,''))
      this.btnname = "Ajouter"
    }

    else {
      this.UserService.getUserById(this.route.snapshot.params['updateElement']).
        subscribe(
          (data: any) => {
            setTimeout(()=> {this.user = data;
              console.log(this.user)})

          }
        )

      this.btnname = "Modifier"
    }
  }

  submit() {
    if (this.route.snapshot.params['updateElement'] === "0") {
      this.UserService.AddUser(this.user).subscribe(
        (response:User) => {

          if (response.prenom === "Email") {
            this.loginExistant = false
            this.WarnMsg=response.nom
            this.EmailExistant = true
          }
          else if(response.prenom === "Login"){
            this.EmailExistant = false
            this.WarnMsg=response.nom
            this.loginExistant = true
          }
          else {
            this.messageService.add({severity:'success', summary: 'Success', detail: 'Utilisateur Ajouté avec succes'})
            setTimeout(() => { this.router.navigate(['GLV/primary-module/listUsers'])},2000 );
          }
        }
      );
    }
    else {
      this.UserService.UpdateUserById(this.user).subscribe(
        response => {
          this.messageService.add({severity:'success', summary: 'Success', detail: 'Utilisateur Modifié avec succes'});
          setTimeout(() => {this.router.navigate(['GLV/primary-module/listUsers'])},2000 )
        }
      );
    }

  }
}
