import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UserService, User } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {
  
  users:User[];
  loading: boolean = true;

  constructor(private userService:UserService,
              private route:Router) {
   }

  ngOnInit(): void {
    this.RetrieveAllUsers();
    this.loading = false;
  }


  RetrieveAllUsers(){
    this.userService.getAllUsers().
    subscribe(
      (data: any) => {
          this.users = data;  }
        //this.users = new MatTableDataSource(data)
    )

  }
  Modifier(SelectedUser:User){
    this.route.navigate(['GLV/primary-module/addUser',SelectedUser.id])
  }
  
  Supprimer(SelectedUser){
    this.userService.DeleteUserById(SelectedUser).
    subscribe(
      response => this.RetrieveAllUsers()
    );
  }
  AfficherDetails(SelectedUser){}
}
