import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { Devis } from './devis.service';

@Injectable({
  providedIn: 'root'
})
export class Accessoire {

  constructor(
    public id:number,
    public label:string,
    public prixA:number,
    public devis:Devis
    )
    {  }
   }
   @Injectable
   (
     {
       providedIn: 'root'
     }
   )
   export class AccessoireService {
     constructor(private httpClient: HttpClient) { }

     getAllAccessoire(){
      return this.httpClient
     .get<Accessoire[]>(`${API_URL}/accessoires/GetAllAccessoires`);
   }

   getAccessoireById(id){
    return this.httpClient
   .get<Accessoire>(`${API_URL}/accessoires/getAccessoire/${id}`);
  }

     updateAccessoire(accessoire){
    return this.httpClient
    .put(`${API_URL}/accessoires/updateAccessoire`,accessoire);

  }

  }


