import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from '../app.constants';
import { ChargeArret, ChargeArretService } from './charge-arret.service';

@Injectable({
  providedIn: 'root'
})
export class Arret {

  constructor(
    public id:number,
    public nbArret:String,

  ) { }
}
@Injectable
(
  {
    providedIn: 'root'
  }
)
export class ArretService {
  constructor(private httpClient: HttpClient) { }

  getAllArret(){
    return this.httpClient
   .get<Arret[]>(`${API_URL}/arrets/GetAllArrets`);
 }
 getArretById(id){
  return this.httpClient
 .get<Arret>(`${API_URL}/arrets/getArret/${id}`);
}

UpdateCharge(charge){
  return this.httpClient
 .put(`${API_URL}/arrets/updateArret`,charge);
}

}
