import { TestBed } from '@angular/core/testing';

import { CabineService } from './cabine.service';

describe('CabineService', () => {
  let service: CabineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CabineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
