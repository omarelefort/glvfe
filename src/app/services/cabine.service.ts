import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { Devis } from './devis.service';

@Injectable({
  providedIn: 'root'
})
export class Cabine {

  constructor(
    public id:number,
    public modelCabine:string,
    public prix:number,
    public provenance:string,
    public devis: Devis,
    public plafond:string,
    public sol:string,
    public miroir:string
  ) { }
}

@Injectable
(
  {
    providedIn: 'root'
  }
)
export class CabineService {
  constructor(private httpClient: HttpClient) { }

  getAllCabine(){
    return this.httpClient
   .get<Cabine[]>(`${API_URL}/cabine/GetAllCabines`);
 }
 getCabineById(id){
  return this.httpClient
 .get<Cabine>(`${API_URL}/cabine/getCabine/${id}`);
}
  getCabineByProvenance(Provenance){
    return this.httpClient
      .get<Cabine>(`${API_URL}/cabine/getCabineByProvenance/${Provenance}`);
  }

UpdateCabine(cabine){
  return this.httpClient
 .put(`${API_URL}/cabine/updateCabine`,cabine);
}

}
