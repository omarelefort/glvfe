import { TestBed } from '@angular/core/testing';

import { ChargeArretService } from './charge-arret.service';

describe('ChargeArretService', () => {
  let service: ChargeArretService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChargeArretService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
