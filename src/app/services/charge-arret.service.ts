import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Charge } from './charge.service';
import { Arret } from './arret.service';
import { Devis } from './devis.service';

@Injectable({
  providedIn: 'root'
})
export class ChargeArret {

  constructor(
    public id:number,
    public prixCh:number,
    public charge:Charge,
    public arret:Arret,
  ) { }
}

@Injectable
(
  {
    providedIn: 'root'
  }
)
export class ChargeArretService {
  constructor(private httpClient: HttpClient) { }

  getAllchargeArret(){
    return this.httpClient
   .get<ChargeArret[]>(`${API_URL}/chargeArret/AllChargeArrets`);
 }
 getchargeArretById(id){
  return this.httpClient
 .get<ChargeArret>(`${API_URL}/chargeArret/getChargeArrets/${id}`);
}
  getchargeArretByChargeArret(idcharge,idarret){
    return this.httpClient
      .get<ChargeArret>(`${API_URL}/chargeArret/ByChargeArrets/${idcharge}/${idarret}`);
  }

UpdatechargeArret(charge){
  return this.httpClient
 .put(`${API_URL}/chargeArret/updateChargeArret`,charge);
}

}
