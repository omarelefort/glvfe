import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { TypePorte } from './type-porte.service';

@Injectable({
  providedIn: 'root'
})
export class Charge {

  constructor(
    public id:number,
    public chargeUtile:string,
    public prixCharge:number,
    public dimensionCabine:string,
    public passagePorte:string,
    public typePorte:TypePorte
  ) { }
}
@Injectable
(
  {
    providedIn: 'root'
  }
)

export class ChargeService {
  constructor(private httpClient: HttpClient) { }


  getAllCharges(){
    return this.httpClient
   .get<Charge[]>(`${API_URL}/charge/GetAllCharges`);
 }
 getChargeById(id){
  return this.httpClient
 .get<Charge>(`${API_URL}/charge/getChargeByID/${id}`);
}

UpdateCharge(charge){
  return this.httpClient
 .put(`${API_URL}/charge/updateCharge`,charge);
}

}
