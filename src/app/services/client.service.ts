import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { Devis } from './devis.service';

@Injectable({
  providedIn: 'root'
})
export class Client {

  constructor(
    public id:number,
    public type:string,
    public nom:string,
    public adresse:string,
    public cp_ville:string,
    public pays:string,
    public code_ice:string,
    public code_if:string,
    public telephone:string,
    public gsm:string,
    public fax:string,
    public email:string,
    public site:string,
    public condition_reglement:string,
    public note:string,
    public texte:string,
    public date_creation:Date,
    public categorie:string,
    public activite:string,
    public region:string,
    public origine:string,
    public suivi:string,
    public recouvreur:string,
    public devise:string,
    public compte_tiers:string,
    public compte_comptable:string,

  ) { }
}
@Injectable
(
  {
    providedIn: 'root'
  })

  export class ClientService {
    constructor(private httpClient: HttpClient) { }

    getAllClient(){
      return this.httpClient
     .get<Client[]>(`${API_URL}/client/allClients`);
   }

   AddClient(client){
    return this.httpClient
   .post(`${API_URL}/client/addClient`,client);
 }

 ModifierClient(client){
  return this.httpClient
 .put(`${API_URL}/client/ModClient`,client);
}

getClientById(clientid){
  return this.httpClient
 .get<Client>(`${API_URL}/client/getClientById/${clientid}`);
}

DeleteClient(clientid){
  return this.httpClient
 .delete<Client>(`${API_URL}/client//DeleteClient/{id}`,clientid);
}
  }
