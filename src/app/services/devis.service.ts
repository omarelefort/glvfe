import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { Client } from './client.service';
import { User } from './user.service';
import {Arret} from "./arret.service";
import {Charge} from "./charge.service";
import {ChargeArret} from "./charge-arret.service";
import {Cabine} from "./cabine.service";
import {TypePorte} from "./type-porte.service";
import {Accessoire} from "./accessoire.service";

export class Devis {
  constructor(
    public id:number,
    public matricule:string,
    public dateDevis : string,
    public typeInstall:string,
    public course:number,
    public vitesse:string,
    public hauteurSousDalle:number,
    public cuvette:number,
    public modeEntrainement:string,
    public localMachinerie:string,
    public acces:string,
    public dimensionGaineL:string,
    public dimensionGaineP:string,
    public passage:string,
    public finition:string,
    public etat:string,
    public prix:number,
    public delaiLivraison:string,
    public garantie:string,
    public entretien:string,
    public user:User,
    public client:Client,
    public chargeArrets:ChargeArret,
    public cabine:Cabine,
    public typePorte:TypePorte,
    public accessoires:Accessoire[],
    public  typeGaine: string,
    public  usageAsc:string,
    public  typeManoeuvre:string,
    public  alimentationMoteur:string,
    public quantite:number,
    public  plafond:string,
    public  sol:string,
    public  miroir:string,
    public ouverture:string,

  ) { }
}
@Injectable({
  providedIn: 'root'
})
export class DevisService {

  constructor(private httpClient: HttpClient) { }

  getAllDevis(){
    return this.httpClient
   .get<Devis[]>(`${API_URL}/devis/allDevis`);
 }
 getDevisById(devisid){
  return this.httpClient
 .get<Devis>(`${API_URL}/devis/getDevis/${devisid}`);
}
DeleteDevisById(devisid){
  return this.httpClient
 .delete(`${API_URL}/devis/deleteDevis/${devisid}`);
}
UpdateDevisById(devis){
  return this.httpClient
 .put(`${API_URL}/devis/updateDevis`,devis);
}
AddDevis(devis){
  return this.httpClient
 .post(`${API_URL}/devis/addDevis`,devis);
}
}
