import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class GetApisService {
  constructor(private httpClient: HttpClient) { }

  getPosts(urlapi:String){
     return this.httpClient
    .get(`${API_URL}/${urlapi}`);
  }
}
