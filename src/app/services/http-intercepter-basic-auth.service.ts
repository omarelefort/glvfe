import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HttpIntercepterBasicAuthService implements HttpInterceptor {

  constructor(
    private AuthSrevice:AuthenticationService
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler){

    let header = this.AuthSrevice.getAuthenticatedToken()
    let username = this.AuthSrevice.getAuthenticatedUser()

    if(header && username){
    req = req.clone({
      setHeaders : {
        Authorization : header
      }
    })
  }
    return next.handle(req)
  }
}