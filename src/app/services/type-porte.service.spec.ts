import { TestBed } from '@angular/core/testing';

import { TypePorteService } from './type-porte.service';

describe('TypePorteService', () => {
  let service: TypePorteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypePorteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
