import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypePorte{

  constructor(
    public id:number,
    public porteCabine:string,
    public prixTypeP:number
  ) { }
}
@Injectable
(
  {
    providedIn: 'root'
  }
)
export class TypePorteService {
  constructor(private httpClient: HttpClient) { }

  addTypePorte(typePorte){
    return this.httpClient
   .post(`${API_URL}/typePortes/addTypePorte`,typePorte);
  }

  getAllTypePorte(){
    return this.httpClient
   .get<TypePorte[]>(`${API_URL}/typePortes/getAllTypePortes`);
 }
 getTypePortesById(id){
  return this.httpClient
 .get<TypePorte>(`${API_URL}/typePortes/getTypePortes/${id}`);
}

UpdateTypePorte(TypePorte){
  return this.httpClient
 .put(`${API_URL}/typePortes/updateTypePorte`,TypePorte);
}

}