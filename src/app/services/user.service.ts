import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';

export class Role {
  constructor(public id:number,
              public role:string,){}
}
export class User {
  constructor(
    public id:number,
    public username:string,
    public nom:string,
    public prenom:string,
    public email:string,
    public password:string,
    public role:Role,
  ) { }
}
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getAllUsers(){
    return this.httpClient
   .get<User[]>(`${API_URL}/utilisateurs/allUsers`);
 }
 getUserById(userid){
  return this.httpClient
 .get<User>(`${API_URL}/utilisateurs/searchUser/${userid}`);
}
getUserByUsername(username){
  return this.httpClient
 .get<User>(`${API_URL}/utilisateurs/searchUserByUsername/${username}`);
}
DeleteUserById(userid){
  return this.httpClient
 .delete(`${API_URL}/utilisateurs/deleteUser/${userid}`);
}
UpdateUserById(User){
  return this.httpClient
 .put(`${API_URL}/utilisateurs/updateUser`,User);
}
AddUser(User){
  return this.httpClient
 .post(`${API_URL}/utilisateurs/addUtilisateur`,User);
}
getAllRoles(){
  return this.httpClient
 .get<Role[]>(`${API_URL}/roles/allRoles`);
}
}
